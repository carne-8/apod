module App

open System

open Fable
open Fable.Core
open Fable.Core.JsInterop

open Browser.Dom
open Browser.CssExtensions

open Thoth.Fetch
open Types

type APODResponse =
    {
        date: String
        explanation: String
        hdurl: String
        media_type: String
        service_version: String
        title: String
        url: String
    }

type Dimensions =
    {
        Width: float
        Height: float
    }

[<Emit("require(\"electron\")")>]
let electron: Electron.AllElectron = jsNative

[<Emit("new Image()")>]
let createImageElement() : Browser.Types.HTMLImageElement = jsNative

[<Emit("document.querySelector(\"html\").style")>]
let htmlStyle: Browser.Types.CSSStyleDeclaration = jsNative

let rec gcd a b =
    if b = 0 then a
    else
        gcd b (a % b)

let reduce a b gdc =
    [ (a / gdc); (b / gdc) ]

let imageDiv = document.getElementById "image"

let fetchNasaAPI() : JS.Promise<Result<APODResponse, FetchError>> =
    promise {
        let url = sprintf "https://api.nasa.gov/planetary/apod?api_key=ZDe7zsmRkZdJ6NscNU6hwLySIA6woA2X7O8042xI"
        
        return! Fetch.tryGet(url)
    }


let setWindowDimensions (width: float) (height: float) =
    //? set body dimensions
    document.body.style.width <- sprintf "%spx" (width.ToString())
    document.body.style.height <- sprintf "%spx" (height.ToString())

    //? set html dimensions
    htmlStyle.width <- sprintf "%spx" (width.ToString())
    htmlStyle.height <- sprintf "%spx" (height.ToString())

    //? change window dimensions
    electron.remote.getCurrentWindow().setSize(int width, int height, true)

let getDimensionsImageFromURL url (callback: int -> int -> unit) =
    let img = createImageElement()
    img.src <- url

    img.addEventListener("load", (fun _ ->
        img.remove()

        callback (int img.naturalWidth) (int img.naturalHeight)
    ))


promise {
    let! nasaAPIResponse = fetchNasaAPI()

    match nasaAPIResponse with
    | Error (NetworkError _) ->
        let errorDiv = document.getElementById("error_message")
        errorDiv.innerText <- "Vérifiez votre connexion internet."
        errorDiv.style.display <- "flex"

        setWindowDimensions 230. 120.

        document.getElementById("content").style.flexDirection <- "column"
        document.getElementById("content").style.alignItems <- "center"
        document.getElementById("content").style.justifyContent <- "space-evenly"
        
        let closeButton = document.getElementById("close_icon")
        closeButton.style.position <- "unset"
        closeButton.onclick <- fun _ ->
            electron.remote.app.quit()
    | Error (FetchFailed _)
    | Error (DecodingFailed _)
    | Error (PreparingRequestFailed _) ->
        let errorDiv = document.getElementById("error_message")
        errorDiv.innerText <- "Il y a eu un problème."
        errorDiv.style.display <- "flex"

        setWindowDimensions 230. 120.

        document.getElementById("content").style.flexDirection <- "column"
        document.getElementById("content").style.alignItems <- "center"
        document.getElementById("content").style.justifyContent <- "space-evenly"
        
        let closeButton = document.getElementById("close_icon")
        closeButton.style.position <- "unset"
        closeButton.onclick <- fun _ ->
            electron.remote.app.quit()
    | Ok x ->
        getDimensionsImageFromURL x.hdurl (fun imageWidth imageHeight ->

            let ratio = reduce imageWidth imageHeight (gcd imageWidth imageHeight)

            let mutable correctedWidth = float imageWidth
            let mutable correctedHeight = imageHeight

            if imageHeight > 700 then
                correctedHeight <- 700
                correctedWidth <- (700.0 * float ratio.[0] / float ratio.[1])

            setWindowDimensions correctedWidth (float correctedHeight)

            let loader = document.getElementById("loader")
            loader.style.display <- "none"

            let content = document.getElementById("content")
            content.style.display <- "flex"

            imageDiv.setAttribute("src", x.hdurl)
            imageDiv.style.height <- sprintf "%spx" (correctedHeight.ToString())
            imageDiv.style.width <- sprintf "%spx" (correctedWidth.ToString())

            let downloadLink = document.getElementById("download")
            downloadLink.setAttribute("href", x.hdurl)
            downloadLink.style.display <- "flex"

            let closeButton = document.getElementById("close_icon")
            closeButton.style.display <- "flex"
            closeButton.onclick <- fun _ ->
                electron.remote.app.quit()

            let copyButton = document.getElementById("copy_link")
            copyButton.style.display <- "flex"
            copyButton.onclick <- fun _ ->
                electron.clipboard.writeText(x.hdurl)
        )
} |> Promise.start