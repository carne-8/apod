namespace Types

module NodeJs =
    type IEnv =
        abstract NASA_API_KEY: string

    type IProcess =
        abstract env: IEnv

    type IDotenv =
        abstract config: unit -> unit

    type IPackageJson =
        {
            version: string
            version_name: string
        }